from wildgram import wildgram
import os
import json
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.stats as stats

def wildnoise(text, ranges):
    indexes = set(range(len(text)))
    for rn in ranges:
        indexes = indexes.difference(range(rn[0], rn[1]))
    indexes = sorted(list(indexes))
    prev = 0
    ret = []
    for i in range(len(indexes)-1):
        if indexes[i] != indexes[i+1]-1:
            ret.append((indexes[prev], indexes[i]+1))
            prev = i+1
            continue
    if prev != len(indexes) -1:
        ret.append((indexes[prev], indexes[len(indexes)-1]+1))

    return ret


def bestOverlap(start, end, ranges):
    max = 0
    maxOption = (0, 0,-3)
    corrset = set(range(int(start), int(end)))
    for i in range(len(ranges)):
        option = ranges[i].split("|!|")
        if len(option) < 3:
            continue
        punset =set(range(int(option[0]), int(option[1])))
        diff = len(corrset.intersection(punset))/len(corrset.union(punset))
        if diff > max:
            max = diff
            maxOption = (option[0], option[1], i)
    return maxOption, max

def parentRankedList():
    start = time.time()

    textfiles = os.listdir("text")
    if not os.path.exists('groups'):
        os.makedirs('groups')
    f = open("groups/rankedlist.txt", "w")
    tot = 0
    for file in textfiles:
        text = open("text/"+file).read()
        annot = open("annotations/"+file+".txt").read().split("\n")
        runRankedList(annot, f, file, text)
    f.close()

def runRankedList(annot, f, file, text):
    ranks = {}
    ranks["root"] = "root"
    ranks["\n"] = [":\n", "root"]
    ranks[":\n"] = ["\n", "root"]
    ranks[": "] = ["\n", ". ", ";"]
    ranks[". "] = ["\n"]
    ranks[","] = [";", ". ", "\n", "|", "-"]
    ranks[";"] = [". ", "\n"]
    ranks["-"] = [";", ". ", "\n", ",", "|"]
    ranks["|"] = [";", ". ", "\n", ",", "|"]
    ranks["and"] = [";", ". ", "\n", "|", "-"]
    ranks["or"] = [";", ". ", "\n", "|", "-"]
    ranks["by"] = [";", ". ", "\n", ",", "|", "-"]
    ranks["in"] = [";", ". ", "\n", ",", "|", "-"]
    ranks["on"] = [";", ". ", "\n", ",", "|", "-"]
    ranks["was"] = [";", ". ", "\n", ",", "|", "-"]
    ranks["is"] = [";", ". ", "\n", ",", "|", "-"]

    order = [":\n", "\n", ". ", ";", ": ", ",", "-", "and", "or", "|", "by", "in", "on", "was", "is"]

    lastKnown = {}
    lastKnown["root"] = -1
    lastKnown["\n"] = -1
    lastKnown[":\n"] = -1
    lastKnown[": "] = -1
    lastKnown[". "] = -1
    lastKnown[","] = -1
    lastKnown[";"] = -1
    lastKnown["-"] = -1
    lastKnown["and"] = -1
    lastKnown["or"] = -1
    lastKnown["|"] = -1
    lastKnown["by"] = -1
    lastKnown["in"] = -1
    lastKnown["on"] = -1
    lastKnown["was"] = -1
    lastKnown["is"] = -1
    prevNewLine = -1
    for i in range(len(annot)):
        option = annot[i].split("|!|")
        if len(option) < 2:
            continue
        if option[2] != "noise":
            prev = np.amax(np.array(list(lastKnown.values())))
            f.write(file+"|!|"+str(prev)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue
        snip = text[int(option[0]):int(option[1])]
        done = False
        for ord in order:
            if ord in snip:
                prev = np.amax(np.array(list([lastKnown[r] for r in ranks[ord]])))
                f.write(file+"|!|"+str(prev)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
                lastKnown[ord] = i
                done = True
                break
        if done:
            continue
        prev = np.max(np.array(list(lastKnown.values())))
        f.write(file+"|!|"+str(prev)+"|!|"+str(i)+"|!|"+annot[i]+"\n")


def parentBasic():
    textfiles = os.listdir("text")
    if not os.path.exists('groups'):
        os.makedirs('groups')
    f = open("groups/basicnewline.txt", "w")
    for file in textfiles:
        annot = open("annotations/"+file+".txt").read().split("\n")
        runBasic(annot, f, file)

    f.close()

def runBasic(annot, f, file):
    prevNewLine = -1
    for i in range(len(annot)):
        option = annot[i].split("|!|")
        if len(option) < 2:
            continue

        if "<NL>" in annot[i]:
            f.write(file+"|!|"+str(-1)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            prevNewLine = i
            continue
        f.write(file+"|!|"+str(prevNewLine)+"|!|"+str(i)+"|!|"+annot[i]+"\n")

def parentBasicAndPeriod():
    textfiles = os.listdir("text")
    if not os.path.exists('groups'):
        os.makedirs('groups')
    f = open("groups/newlineandperiod.txt", "w")
    for file in textfiles:
        annot = open("annotations/"+file+".txt").read().split("\n")
        runBasicAndPeriod(annot, f, file)
    f.close()

def runBasicAndPeriod(annot, f, file):
    prevNewLine = -1
    prevPeriod = -1
    for i in range(len(annot)):
        option = annot[i].split("|!|")
        if len(option) < 2:
            continue


        if "<NL>" in annot[i]:
            prevNewLine = i
            f.write(file+"|!|"+str(-1)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue
        if ". " in annot[i]:
            prevPeriod = i
            f.write(file+"|!|"+str(prevNewLine)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue
        f.write(file+"|!|"+str(np.max([prevNewLine, prevPeriod]))+"|!|"+str(i)+"|!|"+annot[i]+"\n")

def parentBasicAndPeriodAndInner():
    textfiles = os.listdir("text")
    if not os.path.exists('groups'):
        os.makedirs('groups')
    f = open("groups/newlineandperiodandinner.txt", "w")
    for file in textfiles:
        annot = open("annotations/"+file+".txt").read().split("\n")
        runBasicAndPeriodInner(annot, f, file)
    f.close()

def runBasicAndPeriodInner(annot, f, file):
    prevNewLine = -1
    prevPeriod = -1
    prevInner = -1
    for i in range(len(annot)):
        option = annot[i].split("|!|")
        if len(option) < 2:
            continue
        if option[2] != "noise":
            nl = np.max([prevNewLine, prevPeriod, prevInner])
            f.write(file+"|!|"+str(nl)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue
        if "<NL>" in annot[i]:
            prevNewLine = i
            f.write(file+"|!|"+str(-1)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue

        if ". " in annot[i]:
            prevPeriod = i
            f.write(file+"|!|"+str(prevNewLine)+"|!|"+str(i)+"|!|"+annot[i]+"\n")
            continue
        if "," in annot[i]:
            prevInner = i
        if ";" in annot[i]:
            prevInner = i
        if "or" in annot[i]:
            prevInner = i
        if "and" in annot[i]:
            prevInner = i

        f.write(file+"|!|"+str(prevPeriod)+"|!|"+str(i)+"|!|"+annot[i]+"\n")


def calculateGroups(file="rawdata.txt", splitter="||"):
    data = open(file).read().split("\n")
    ret = {}
    for dat in data:
        options = dat.split(splitter)
        if len(options) < 2:
            continue
        #skips noise tokens
        if "noise" in options:
            continue
        if options[0] not in ret:
            ret[options[0]] = {}
        if options[1] not in ret[options[0]]:
            ret[options[0]][options[1]] = []

        ret[options[0]][options[1]] = ret[options[0]][options[1]] + list(range(int(options[3]), int(options[4])))
    for key in ret:
        for k in ret[key]:
            ret[key][k] = list(set(ret[key][k]))
    return ret


def calculateGroupIndividuals(file="bestmatchrawdata.txt", splitter="||"):
    data = open(file).read().split("\n")
    ret = {}
    for dat in data:
        options = dat.split(splitter)
        if len(options) < 2:
            continue
        #skips noise tokens
        if "noise" in options:
            continue
        if options[0] not in ret:
            ret[options[0]] = {}
        if options[1] not in ret[options[0]]:
            ret[options[0]][options[1]] = []

        ret[options[0]][options[1]].append(options[5])
    return ret

def getIndexesFromText(indexes, text):
    ret = ""
    prev = -1
    indexes = sorted(indexes)
    for index in indexes:
        if index != prev + 1:
            if prev != -1:
                ret = ret +"<MISSING>"
        ret = ret + text[index]
        prev = index
    return ret.replace("\n", "<NL>")


## simple test (group by new line)
def calculateBestMatchGroup():
    raw = calculateGroups()
    if not os.path.exists('experiments'):
        os.makedirs('experiments')

    files = os.listdir("groups")
    data = []
    print("Method & Average Score & Median Score")
    for f in files:
        experiment = open("experiments/"+f, "w")
        file = calculateGroups("groups/"+f, "|!|")
        ret = []
        for doc in raw:
            text = open("text/"+doc).read()
            if doc not in file:
                print("WTF a missing doc", doc, f)
                continue
            rawgroups = raw[doc]
            filegroups = file[doc]
            for group in rawgroups:
                indexes = set(rawgroups[group])
                max = 0
                bestIndexes = []
                for filegroup in filegroups:
                    fileindexes = set(filegroups[filegroup])
                    diff = len(fileindexes.intersection(indexes))/len(fileindexes.union(indexes))
                    if diff > max:
                        max = diff
                        bestIndexes = fileindexes
                ret.append(max)
                experiment.write(doc + "|!|" + getIndexesFromText(indexes, text) + "|!|" + getIndexesFromText(bestIndexes, text)+"|!|"+ str(max) + "\n")
        print(f + " & " + str(np.average(ret)) + " & " + str(np.median(ret)))
        data.append(ret)
        experiment.close()
    plt.hist(data, bins=20, label=files)
    plt.legend()
    plt.xlabel("Best Jaccard Similarity Index")
    plt.ylabel("Frequency in Dataset")
    plt.title("Best Jaccard Similarity Index Per Group vs. Frequency for n2c2 Medication Dataset (2009)")
    plt.show()







def getLineages(id, lines):
    dicty = {}
    indexes = []
    found = False
    for line in lines:
        options = line.split("|!|")
        if len(options) <2:
            continue
        if int(options[0]) != int(id):
            if found:
                break
            continue
        found = True
        dicty[int(options[2])] = int(options[1])
        indexes.append(int(options[2]))

    ret = {}
    for index in indexes:
        start = index
        lineage = [int(start)]
        while start != -1:
            lineage.append(int(dicty[int(start)]))
            start = int(dicty[int(start)])
        ret[index] = lineage
    return ret







def applyBaseWildgram():
    textfiles = os.listdir("text")
    if not os.path.exists('annotations'):
        os.makedirs('annotations')
    for file in textfiles:
        text = open("text/"+file).read()
        annot = open("annotations/"+file+".txt", "w")
        print("applying file...", file)
        ranges = wildgram(text, returnNoise=True)
        for i in range(len(ranges)):
            annot.write(str(ranges[i]["startIndex"])+"|!|"+str(ranges[i]["endIndex"])+
            "|!|"+ranges[i]["tokenType"]+ "|!|" + ranges[i]["token"].replace("\n", "<NL>")+ "\n")
        annot.close()


def calculatedBestMatchPrecisionRecall(threshold=1.0):
    files = os.listdir("experiments")
    recall = []
    for f in files:
        experiment = open("experiments/"+f).read().split("\n")
        ret = {}
        ret["tp"] = 0
        ret["tot"] = 0
        for line in experiment:
            options = line.split("|!|")
            if len(options) < 2:
                continue
            ret["tot"] = ret["tot"] + 1
            if float(options[-1]) >= threshold:
                ret["tp"] =ret["tp"] +1
        recall.append(ret["tp"]/ret["tot"])
    plt.bar(files, recall)
    plt.title("Recall at threshold: "+str(threshold) +" for different methods on n2c2 2009 dataset.")
    plt.xlabel("Methods")
    plt.ylabel("Recall")
    plt.show()


def cacheBestMatchIndividual():
    individuals = open("rawdata.txt").read().split("\n")
    f = open("bestmatchrawdata.txt", "w")
    for individual in individuals:
        options = individual.split("||")
        if len(options) < 2:
            continue
        annot = open("annotations/"+options[0]+".txt").read().split("\n")
        bestSample, score = bestOverlap(options[3], options[4], annot)
        f.write(individual + "||" + str(bestSample[2])+"||" + str(score) + "\n")
    f.close()

def plotDistances():
    f = open("InterIntraMaxMin.json")
    data = json.load(f)
    ret = []
    labels = []
    for method in data:
        ret.append(np.array(data[method]["x"])-np.array(data[method]["y"]))
        labels.append(method+" Difference in Distance")

    plt.hist(ret, bins = 10, label=labels)
    plt.legend()
    plt.title("Inter - Min Intra - Max Difference Histogram for Different Methods")
    plt.show()

def calculateInterIntraOverlapRaw(filename = "InterIntraRaw.json"):
    ## create dictionary for groups
    dicty = calculateGroupIndividuals()
    data = {}
    ## for each individual
    for methodFile in os.listdir("groups"):
        meth = open("groups/"+methodFile).read().split("\n")
        data[methodFile] = {}
        x = []
        y = []
        for file in dicty:
            print(methodFile, file, "...")
            lineages = getLineages(file, meth)
            data[methodFile][file] = {}
            for group in dicty[file]:
                data[methodFile][file][group] = {}
                inter = []
                intra = []
                for individual in dicty[file][group]:
                    print(methodFile, file, individual, "...")
                    if int(individual) < 0:
                        continue
                    lin1 = lineages[int(individual)]
                    ### determine average intergroup distance (using method)
                    for individual2 in dicty[file][group]:
                        if int(individual2) < 0:
                            continue
                        lin2 = lineages[int(individual2)]
                        intra.append(calculateLineageMatch(lin1, lin2))

                    for group2 in dicty[file]:
                        if group == group2:
                            continue
                        for individual2 in dicty[file][group2]:
                            if int(individual2) < 0:
                                continue
                            lin2 = lineages[int(individual2)]
                            inter.append(calculateLineageMatch(lin1, lin2))

                data[methodFile][file][group]["inter"] = inter
                data[methodFile][file][group]["intra"] = intra
    f = open(filename ,"w")
    json.dump(data, f)
    f.close()
#### calculating the distance within group vs. intra groupe
###
def calculateInterIntraOverlap(funcIntra = np.average, funcInter = np.average, filename = "InterIntraAverage.json"):
    ## create dictionary for groups
    dicty = calculateGroupIndividuals()
    data = {}
    ## for each individual
    for methodFile in os.listdir("groups"):
        meth = open("groups/"+methodFile).read().split("\n")
        data[methodFile] = {}
        x = []
        y = []
        for file in dicty:
            print(methodFile, file, "...")
            lineages = getLineages(file, meth)
            for group in dicty[file]:
                for individual in dicty[file][group]:
                    print(methodFile, file, individual, "...")
                    inter = []
                    intra = []
                    if int(individual) < 0:
                        continue
                    lin1 = lineages[int(individual)]
                    ### determine average intergroup distance (using method)
                    for individual2 in dicty[file][group]:
                        if int(individual2) < 0:
                            continue
                        lin2 = lineages[int(individual2)]
                        intra.append(calculateLineageMatch(lin1, lin2))

                    for group2 in dicty[file]:
                        if group == group2:
                            continue
                        for individual2 in dicty[file][group2]:
                            if int(individual2) < 0:
                                continue
                            lin2 = lineages[int(individual2)]
                            inter.append(calculateLineageMatch(lin1, lin2))
                    x.append(float(funcIntra(intra)))
                    y.append(float(funcInter(inter)))

        data[methodFile]["x"] = x
        data[methodFile]["y"] = y
    f = open(filename ,"w")
    json.dump(data, f)
    f.close()
    for method in data:
        data[method]["x"] = np.array(data[method]["x"])
        data[method]["y"] = np.array(data[method]["y"])
    print("Methods:", data.keys())
    print("Inter Function:", funcInter)
    print("Intra Function:", funcIntra)
    print("Statistics of each Method")
    print("Method", "&", "Variance Intra", "&",  "Variance Inter", "&" ,"Ratio","&" , "T-Test", "\\\\" )
    for method in data:
        print(method, "&", np.var(data[method]["x"]), "&", np.var(data[method]["y"]), "&",np.var(data[method]["y"])/np.var(data[method]["x"]), "&", stats.ttest_ind(a=data[method]["x"], b=data[method]["y"], equal_var=False), "\\\\")

    print("Inter- and Intra- Distance for Different Methods \\\\")
    print("Method & Intra Average & Inter Average & Intra Median & Inter Median \\\\ ")


    for method in data:
        print(method, "&", np.average(data[method]["x"]),"&",np.average(data[method]["y"]),"&",np.median(data[method]["x"]),"&",np.median(data[method]["y"]), "\\\\")
        plt.plot(data[method]["x"], data[method]["y"], "o", label=method)
    plt.legend()
    plt.title("Average Inter- and Intra- Distance Between Groups based on Tree Nodes, different methods")
    plt.xlabel("Average Intra Distance: " + str(funcIntra))
    plt.ylabel("Average Inter Distance: " + str(funcInter))
    plt.show()
                ### determine average intragroup distance
                ### record as pair
                ### plot points
def calculateLineageMatch(lin1, lin2):
    return len(set(lin1).difference(set(lin2)))+len(set(lin2).difference(set(lin1)))

def plotThresholdPrecisionRecall(threshold=3.0):
    f = open("InterIntraRaw.json")
    data = json.load(f)
    precision = []
    recall = []
    for method in data:
        prec = []
        rec = []
        for doc in data[method]:
            for group in data[method][doc]:
                inter = np.array(data[method][doc][group]["inter"])
                intra = np.array(data[method][doc][group]["intra"])
                tp = intra[intra <= threshold]
                fp = inter[inter <= threshold]
                tot = len(tp) + len(fp)
                if tot != 0:
                    prec.append(len(tp)/(len(tp) + len(fp)))
                else:
                    prec.append(0.0)
                if len(intra) != 0:
                    rec.append(len(tp)/(len(intra)))
                else:
                    rec.append(0.0)
        precision.append(np.average(prec))
        recall.append(np.average(rec))

    print("Precision and Recall For Different Distance Thresholds")
    print("Method & Threshold & Precision & Recall\\\\")
    for i in range(len(list(data.keys()))):
        print(list(data.keys())[i], "&", threshold, "&", precision[i], "&", recall[i], "\\\\")
    #plt.bar(data.keys(), precision)
    '''
    ind = np.arange(len(data.keys()))
    width = 0.35
    plt.bar(ind,recall, label="Recall")
    plt.bar(ind+0.35, precision, label="Precision")
    plt.xticks(ind + width / 2, data.keys())
    plt.legend()
    plt.title("Precision and Recall, Different Methods, Threshold of "+str(threshold))
    plt.show()'''


#### calculate jaccard distance for pure annotation abstraction
def calculateBestMatchAnnotations():
    data = open("rawdata.txt").read().split("\n")
    ret = []
    for dat in data:
        options = dat.split("||")
        if len(options) < 2:
            continue
        annot = open("annotations/"+options[0]+".txt").read().split("\n")
        bestSample, score = bestOverlap(options[3], options[4], annot)
        ret.append(score)
    ret = np.array(ret)
    print(len(ret[ret == 1.0])/len(ret))
    print(len(ret[ret > 0.95])/len(ret))
    print(len(ret[ret > 0.9])/len(ret))
    print(len(ret[ret > 0.8])/len(ret))
    print(np.average(ret))
    print(np.median(ret))
    plt.hist(ret, bins=20)
    plt.xlabel("Best Jaccard Similarity Index")
    plt.ylabel("Frequency in Dataset")
    plt.title("Best Jaccard Similarity Index vs. Frequency in Dataset for n2c2 Medication Dataset (2009)")
    plt.show()

def calculateValues():
    data = open("bestmatchrawdata.txt").read().split("\n")
    vals = [v.split("||")[-1] for v in data][:-1]
    ret = np.array([float(v) for v in vals])
    print(vals)
    print(len(ret[ret == 1.0])/len(ret))
    print(len(ret[ret > 0.95])/len(ret))
    print(len(ret[ret > 0.9])/len(ret))
    print(len(ret[ret > 0.8])/len(ret))
    print(np.average(ret))
    print(np.median(ret))

print("Make sure you move the text files from 2009 into this folder.")
input("Ready? Any key to continue.")
applyBaseWildgram()
parentBasic()
parentBasicAndPeriod()
parentBasicAndPeriodAndInner()
parentRankedList()
calculateBestMatchGroup()
calculatedBestMatchPrecisionRecall()
calculatedBestMatchPrecisionRecall(0.8)
calculatedBestMatchPrecisionRecall(0.5)
calculateBestMatchAnnotations()
calculateValues()
